#!/bin/bash

# put gh into environment

if [ -d $HOME/gh ]; then
  # gh unpacked in home directory
  alias gh='$HOME/gh/bin/gh'
  export MANPATH=$HOME/gh/share/man:$MANPATH
fi

# enable tab-completion if gh is installed anywhere
hash gh &> /dev/null && eval "$(gh completion -s bash)"
