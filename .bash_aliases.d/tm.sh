#!/bin/bash

# shortening of tmux to open a new session if the requested session does not exist
#   also add tab completion for currently open sessions

# Launch a tmux session with passed name
# - if the session already exists, we attach
#   otherwise we create a new session
# - if no name is provided and only one session is active,
#   we attach to that session.
#   attached session is attached to (tmux default)
tm() {
  if [ -z $1 ]; then
    # no session name provided
    local _session_count=$(tmux ls 2> /dev/null | wc -l)
    if (( _session_count == 1 )); then
      # exactly one session, attach
      tmux attach
      return $?
    elif (( _session_count > 1 )); then
      # more than one session, list them
      tmux ls
      return $?
    else
      # zero sessions, make a new (unnamed) one
      tmux
    fi
  else
    # name provided, attach if already available, otherwise create
    tmux attach -t $1 || tmux new -s $1
    return $?
  fi
}
# tab-complete for tm
__complete_tm() {
  # disable readline filename completion
  compopt +o default

  if [[ "$COMP_CWORD" = "1" ]]; then
    local __curr_word="${COMP_WORDS[$COMP_CWORD]}"
    local __options="$(tmux ls 2> /dev/null | sed 's/:.*$//g')"
    COMPREPLY=($(compgen -W "${__options}" "${__curr_word}"))
  else
    COMPREPLY=()
  fi
}
complete -F __complete_tm tm
