#!/bin/bash

# connecting to umn computers via the umn CiscoAnyconnect VPN

################################################################################
# __umn_vpn
#   Shell variable storing location of VPN CLI
#   this is where the vpn is installed on Ubuntu 20.04
################################################################################
export __umn_vpn=/opt/cisco/anyconnect/bin/vpn

################################################################################
# __umn_host
#   The host name for our connection to UMN.
#   This was found by connecting to the VPN using the GUI and then using
#   the CLI to see what hosts were already stored in the history.
################################################################################
export __umn_host="UMN Computer Science Full Tunnel"

################################################################################
# __umn_is_connected
#   Check if the VPN is in the 'Connected' state
################################################################################
__umn_is_connected() {
  if ${__umn_vpn} state | grep -q "Connected"; then
    return 0
  else
    return 1
  fi
}

################################################################################
# __umn_open
#   Open the VPN connection. 
#   We don't do anything if the VPN is already in a 'Connected' state.
################################################################################
__umn_open() {
  if __umn_is_connected; then
    echo "VPN already connected. Doing nothing..."
    return 0
  fi

  ${__umn_vpn} connect "${__umn_host}"
  return $?
}

################################################################################
# __umn_close
#   Close the VPN connection
#   We don't do anything if the VPN is not in a 'Connected' state.
################################################################################
__umn_close() {
  if ! __umn_is_connected; then
    echo "VPN not connected to anything. Doing nothing..."
    return 0
  fi

  ${__umn_vpn} disconnect
  return $?
}

################################################################################
# __umn_state
#   Print the state the VPN is in
################################################################################
__umn_state() {
  ${__umn_vpn} state
  return $?
}

################################################################################
# umn
#   The CLI to our wrapper around the cisco anyconnect vpn
################################################################################
__umn_help() {
  cat <<\HELP

 USAGE:
  umn <command>

 COMMANDS:
  help  : Print this message and exit
  open  : Open the VPN connection if not already connected.
          Will require user to type in password and approve a DUO auth.
  close : Disconnect the VPN if connected.
  state : Print the state the VPN is in
  vpn   : Open VPN raw CLI.

HELP
  return 0
}
umn() {
  case $1 in
    open|close|help|state)
      __umn_${1} ${@:2}
      return $?
      ;;
    vpn)
      ${__umn_vpn} ${@:2}
      return $?
      ;;
    *)
      __umn_help
      return $?
  esac
}

# tab completion for umn command
complete -W "open close vpn state help" umn

