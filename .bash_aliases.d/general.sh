
# shorten htop command for me only
alias metop='htop -u $USER'

#####################
# Add .local/bin to PATH
if [ -d $HOME/.local/bin ]; then
  to_add="${HOME}/.local/bin"
  case "${PATH}" in
    *:"${to_add}":*|*:"${to_add}"|"${to_add}":*)
      ;;
    *)
      export PATH=${PATH:+$PATH:}$HOME/.local/bin
      ;;
  esac
fi

##########################
# tmux wrapper around jupyter-notebook
hash jupyter-notebook &> /dev/null && alias jnb='tmux new -s ipynb jupyter-notebook'

# texfot alias if unpacked in home directory
[ -f $HOME/texfot/texfot.pl ] && alias texfot=$HOME/texfot/texfot.pl

# jq alias if downloaded to home directory
[ -f $HOME/jq-linux64 ] && alias jq=$HOME/jq-linux64

# fun shrug
alias shrug='printf "¯\_(ツ)_/¯\n"'

