#!/bin/bash

# Setup safe cp and safe mv, checking that copy succeeded

# A safe copy where we make sure that the copy succeeded
safe-cp() {
  local _file="$1"
  local _dest_dir="$2"
  if [[ ! -d $_dest_dir ]]; then
    echo "'${_dest_dir}' is not a directory to copy to."
    return 127
  fi
  if [[ ! -f $_file ]]; then
    echo "'${_file}' is not a file to copy."
    return 127
  fi
  echo -n "$_file..."
  local _dest=$_dest_dir/$(basename $_file)
  if cp -t $_dest_dir $_file; then
    sync
    if cmp -s $_dest $_file; then
      echo "success!"
      return 0
    else
      echo "failed on cmp"
      #remove non-matching destination file
      rm $_dest
    fi
  else
    echo "failed on cp"
    #remove failed copy (if it exists)
    # sometimes cp leaves a partially copied file behind
    if [[ -f $_dest ]]; then
      rm $_dest
    fi
  fi
  return 1
}

# Safely move a file, making sure the copy succeeded
safe-mv() {
  local _file="$1"
  local _dest_dir="$2"
  if safe-cp $_file $_dest_dir; then
    rm $_file
  fi
}

