#!/bin/bash

# open research environments

###########################
# Setup ldmx dev environment
# Adds a flag at front of PS1 to signal that I am in ldmx environment
ldmx-env() {
  # setup env
  source $HOME/code/ldmx/ldmx-sw/scripts/ldmx-env.sh || return $?
  # grep only in source
  alias grepmodules='grep --exclude-dir=build --exclude-dir=docs --exclude-dir=install --exclude-dir=.git -rHn'
  # add [ldmx] tag
  export PS1="\[$(tput setaf 5)\][ldmx]\[$(tput sgr0)\] $PS1"
  # delete ourselves
  unset -f ldmx-env
}

hps-env() {
  local _old_pwd=$OLDPWD
  cd $HOME/hps
  source env/env.sh hpsrc
  local rc=$?
  cd - &> /dev/null
  export OLDPWD=${_old_pwd}
  export PS1="\[$(tput setaf 5)\][hps]\[$(tput sgr0)\] $PS1"
  unset -f hps-env
  return ${rc}
}
