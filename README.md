# Personal Config Storage

Following [tutorial](https://www.atlassian.com/git/tutorials/dotfiles)
on Atlassian Bitbucket.

## Install on New Computer

```
sudo apt update
sudo apt install curl git vim
git clone --bare git@gitlab.com:tbeichlersmith/config.git $HOME/.cfg
rm .bash_logout .bashrc # DANGEROUS
git --work-tree=$HOME --git-dir=$HOME/.cfg checkout main
config config --local status.showUntrackedFiles no
```
