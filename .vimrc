" REQUIRED. This makes vim invoke Latex-Suite when you open a tex file.
filetype plugin on

" OPTIONAL: Starting with Vim 7, the filetype of empty .tex files defaults to
" 'plaintex' instead of 'tex', which results in vim-latex not being loaded.
" The following changes the default filetype back to 'tex':
let g:tex_flavor='latex'

set tabstop=2 "number of columns a tab counts for
set expandtab "replace tab character with appropriate number of spaces
set shiftwidth=2 "number of spaces for each step of (auto)indent
"set autoindent
"set smartindent

" Enable saving of undo history data from buffer to a file
" Next time vim starts, it will load the undo history
set undofile

" Directory for where to store undo history files
set undodir=~/.vim/undodir

syntax on

" Turn off column one highlight when searching for ^
set nohlsearch

" powerline plugin implementation
" use vim-plug to organize plugins
" use lightline as statusline creator

" Allow for statusline
set laststatus=2

"Turn off showing of mode (displayed in powerline)
set noshowmode

"Change lightline colorscheme to match terminals
"Add gitbranch#name plug in after modified flag
let g:lightline = {
    \ 'colorscheme': 'solarized',
    \ 'active': {
    \   'left': [ [ 'mode', 'paste' ],
    \             [ 'readonly', 'filename' , 'modified' ] ]
    \ }
    \ }

" Map the ':f' executable in normal mode to formatting command
map :f :ClangFormat

" Install vim-plug if it isn't installed on this machine
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" vim-plug installed plugins
call plug#begin('~/.vim/plugged/')

" lightline powerline
Plug 'itchyny/lightline.vim'

" git branch name plugin
Plug 'itchyny/vim-gitbranch'

" documentation skeleton generator
" currently only supports vim --version > 8.0.1630
"Plug 'kkoomen/vim-doge'
"
" google style guid (via clang format)
Plug 'rhysd/vim-clang-format'

" syntax highlighting for typst
Plug 'kaarmu/typst.vim'

call plug#end()
